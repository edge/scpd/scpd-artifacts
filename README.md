# SCPD Artifacts

This repository contains various artifacts generated throughout the study of the Storage Cabinet Physical Design (SCPD) problem introduced in "Mathematical models based on decision hypergraphs for designing a storage cabinet".
Please find the manuscript and the supplementary material in the [HAL archive](https://inria.hal.science/hal-04303041).

## Presentation of files

### Instances

The directory *instances* contains the 180 instances used to benchmarks the formulations.

Instances are described using a file with the following structure:

```
Number of boxes
For each box:
    ID Height Width Length
Number of items
For each item:
    ID Height Width Length Profit Start Duration
```

An example of instance is:

```
1
1 4 4 4
4
1 2 2 2 1 0 2
2 2 2 2 1 1 3
3 2 3 2 1 0 3
4 2 3 2 1 0 3
```

### Solutions

The directory *bestsols* contains the best solution found by the three formulations for each of the 180 instances.

The file *BestSols.csv* is a table containing, for each instance, the formulation that found the best primal bound, the formulation the found the best dual bound, the best primal bound and the best dual bound.

Solution are described using a file with the following structure:

```
Number of used boxes
For every used box:
    ID Number of shelves
    For every shelf:
        ID Height Number of compartments
        For every compartment:
            ID Width Number of assigned items
            For every assigned items:
                ID
```

An example of solution for the instance described above is:

```
1
1 2
1 2 2
1 2 1
1
2 2 1
2
2 2 0
```

### Checker

The file *checker.py* contains a solution checker written in python.
It's usage is `python checker.py <instance file> <solution file>`.

### Results

The file *FormulationC.csv* (resp. *FormulationAF.csv* and *FormulationAF+ALL.csv*) is a table containing, for each instance, the number of variables, the number of constraints, the primal bound, the dual bound, the gap and the solver time used for the formulation.
The formulations based on decision hypergraphs contain an additional column corresponding to the time used to create the decision hypergraph.

