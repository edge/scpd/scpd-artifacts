""" ----------------------------------------------------------------------------

    Checks if a solution is feasible to an instance of the Automatic Storage
    Design problem.

    Such a solution must be verify:
    - The sum of shelves' heights doesn't exceed the height of the box in which
    they are placed.
    - The sum of compartments' widths in every shelf doesn't exceed the width
    of the box in which they are placed.
    - The sum of items' lengths in every compartment at every instant doesn't
    exceed the length of the box to which they are affected.
    - Every item is affected to a compartment at most once.
    - Every item affected to a compartment has an height lower than or equal
    to the compartment's and a width equal to the compartment's width.

---------------------------------------------------------------------------- """

import sys

from statistics import variance, covariance

class Box:
    def __init__(self):
        self.ID = -1
        self.height = 0
        self.width = 0
        self.length = 0

class Item:
    def __init__(self):
        self.ID = -1
        self.boxID = -1
        self.shelfID = -1
        self.compartmentID = -1
        self.height = 0
        self.width = 0
        self.length = 0
        self.profit = 0
        self.start = 0
        self.duration = 0

class Instance:
    def __init__(self):
        self.boxes = {}
        self.items = {}

class Solution:
    def __init__(self):
        self.boxesShelves = {}
        self.boxesShelvesCompartments = {}
        self.boxesShelvesCompartmentsItems = {}

class CheckerOutput:
    def __init__(self):
        self.isFeasible = False
        self.cost = 0

class InstanceIndicators:
    def __init__(self):
        self.nbBoxes = 0
        self.nbItems = 0

        self.minHeightBoxes = 0
        self.meanHeightBoxes = 0
        self.maxHeightBoxes = 0
        self.varianceHeightBoxes = 0

        self.minWidthBoxes = 0
        self.meanWidthBoxes = 0
        self.maxWidthBoxes = 0
        self.varianceWidthBoxes = 0

        self.minLengthBoxes = 0
        self.meanLengthBoxes = 0
        self.maxLengthBoxes = 0
        self.varianceLengthsBoxes = 0

        self.minVolumeBoxes = 0
        self.meanVolumeBoxes = 0
        self.maxVolumeBoxes = 0
        self.totalVolumeBoxes = 0
        self.varianceVolumeBoxes = 0

        self.nbTimePeriods = 0

        self.minHeightItems = 0
        self.meanHeightItems = 0
        self.maxHeightItems = 0
        self.varianceHeightItems = 0

        self.minWidthItems = 0
        self.meanWidthItems = 0
        self.maxWidthItems = 0
        self.varianceWidthItems = 0

        self.minLengthItems = 0
        self.meanLengthItems = 0
        self.maxLengthItems = 0
        self.varianceLengthItems = 0

        self.minProfitItems = 0
        self.meanProfitItems = 0
        self.maxProfitItems = 0
        self.sumProfitsItems = 0
        self.meanProfitItemsPerTimePeriod = 0
        self.varianceProfitItems = 0

        self.minVolumeItems = 0
        self.meanVolumeItems = 0
        self.meanVolumeItemsPerTimePeriod = 0
        self.maxVolumeItems = 0
        self.totalVolumeItems = 0
        self.varianceVolumeItems = 0
        self.volumeItemsPerWidth = {}
        self.varianceVolumeItemsPerWidth = 0

        self.nbHeights = 0
        self.nbWidths = 0
        self.nbItemsPerWidth = {}

        self.covarianceProfitsLengths = 0

        self.nbCliques = 0
        self.nbCliquesPerWidth = {}

        self.nbNonTrivialCliques = 0
        self.nbNonTrivialCliquesPerWidth = {}

        self.minCliqueSize = 0
        self.minCliqueSizePerWidth = {}

        self.meanCliqueSize = 0
        self.meanCliqueSizePerWidth = {}

        self.maxCliqueSize = 0
        self.maxCliqueSizePerWidth = {}

class SolutionIndicators:
    def __init__(self):
        self.minNbShelvesPerBox = 0
        self.meanNbShelvesPerBox = 0
        self.maxNbShelvesPerBox = 0

        self.minNbCompartmentsPerBox = 0
        self.meanNbCompartmentsPerBox = 0
        self.maxNbCompartmentsPerBox = 0

        self.nbAssignedItems = 0
        self.proportionAssignedItems = 0

        self.nbAssignedItemsPerWidth = {}
        self.proportionAssignedItemsPerWidth = {}

        self.nbAssignedItemsInNonTrivialCliques = 0
        self.proporitionAssignedItemsInNonTrivialCliques = 0

        self.volumeAssignedItems = 0
        self.volumeAssignedItemsPerTimePeriod = {}

        self.ratioVolumeAssignedItemsVolumeBoxes = 0
        self.ratioVolumeAssignedItemsVolumeBoxesPerTimePeriod = {}

def printUsage():
    print("Usage: python3 checker.py <instance file> [optional: solution file]")

def readInstance(filename):
    instanceFile = open(filename, 'r')

    instance = Instance()

    nbBoxes = int(instanceFile.readline())

    for _ in range(nbBoxes):
        line = instanceFile.readline()
        tokens = line.split()

        box = Box()
        box.ID = int(tokens[0])
        box.height = int(tokens[1])
        box.width = int(tokens[2])
        box.length = int(tokens[3])

        instance.boxes[box.ID] = box

    nbItems = int(instanceFile.readline())

    for _ in range(nbItems):
        line = instanceFile.readline()
        tokens = line.split()

        item = Item()
        item.ID = int(tokens[0])
        item.height = int(tokens[1])
        item.width = int(tokens[2])
        item.length = int(tokens[3])
        item.profit = int(tokens[4])
        item.start = int(tokens[5])
        item.duration = int(tokens[6])

        instance.items[item.ID] = item

    return instance

def readSolution(filename, instance):
    solutionFile = open(filename, 'r')

    solution = Solution()

    nbBoxesSolution = int(solutionFile.readline())

    for _ in range(nbBoxesSolution):
        line = solutionFile.readline()
        tokens = line.split()

        boxID = int(tokens[0])

        if boxID not in instance.boxes.keys():
            print('Solution error: Box ', boxID, ' isn\'t in the instance file')
            return

        box = instance.boxes[boxID]

        solution.boxesShelves[box.ID] = {}
        solution.boxesShelvesCompartments[box.ID] = {}
        solution.boxesShelvesCompartmentsItems[box.ID] = {}

        nbShelves = int(tokens[1])

        for _ in range(nbShelves):
            line = solutionFile.readline()
            tokens = line.split()

            shelf = Box()
            shelf.ID = int(tokens[0])
            shelf.height = int(tokens[1])
            shelf.width = instance.boxes[boxID].width
            shelf.length = instance.boxes[boxID].length

            solution.boxesShelves[box.ID][shelf.ID] = shelf
            solution.boxesShelvesCompartments[box.ID][shelf.ID] = {}
            solution.boxesShelvesCompartmentsItems[box.ID][shelf.ID] = {}

            nbCompartments = int(tokens[2])

            for _ in range(nbCompartments):
                line = solutionFile.readline()
                tokens = line.split()

                compartment = Box()
                compartment.ID = int(tokens[0])
                compartment.height = shelf.height
                compartment.width = int(tokens[1])
                compartment.lenght = shelf.length

                solution.boxesShelvesCompartments[box.ID][shelf.ID][compartment.ID] = compartment
                solution.boxesShelvesCompartmentsItems[box.ID][shelf.ID][compartment.ID] = []

                nbItemsInShelf = int(tokens[2])

                for _ in range(nbItemsInShelf):
                    line = solutionFile.readline()
                    tokens = line.split()

                    itemID = int(tokens[0])

                    if itemID not in instance.items.keys():
                        print('Solution error: Item ', itemID, ' isn\'t in the instance file')

                    solution.boxesShelvesCompartmentsItems[box.ID][shelf.ID][compartment.ID].append(itemID)

    return solution

def checkSolution(instance, solution):
    isFeasible = True
    cost = 0

    itemAssigned = {}

    for itemID in instance.items:
        itemAssigned[itemID] = False

    for boxID in instance.boxes:
        if boxID in solution.boxesShelves.keys():
            for shelfID in solution.boxesShelves[boxID]:
                for compartmentID in solution.boxesShelvesCompartments[boxID][shelfID]:
                    for itemID in solution.boxesShelvesCompartmentsItems[boxID][shelfID][compartmentID]:
                        if itemAssigned[itemID]:
                            print('Unfeasible: Item ' + str(itemID) + ' has been affected more than once.')
                            isFeasible = False

    for boxID, box in instance.boxes.items():
        sumHeight = 0

        if boxID not in solution.boxesShelves:
            continue

        for shelfID, shelf in solution.boxesShelves[boxID].items():
            sumHeight += shelf.height

            if sumHeight > box.height:
                print('Unfeasible: Box ' + str(boxID) + ' doesn\'t satisfy height constraints.')
                isFeasible = False

            sumWidth = 0

            for compartmentID, compartment in solution.boxesShelvesCompartments[boxID][shelfID].items():
                sumWidth += compartment.width

                if sumWidth > box.width:
                    print('Unfeasible: Shelf ' + str(shelfID) + ' in box ' + str(boxID) + ' doesn\'t satisfy width constraints.')
                    isFeasible = False

                deltaLengths = {}

                for itemID in solution.boxesShelvesCompartmentsItems[boxID][shelfID][compartmentID]:
                    item = instance.items[itemID]

                    if item.start not in deltaLengths:
                        deltaLengths[item.start] = 0

                    if item.start + item.duration not in deltaLengths:
                        deltaLengths[item.start + item.duration] = 0

                    deltaLengths[item.start] += item.length
                    deltaLengths[item.start + item.duration] -= item.length

                    cost += item.profit

                # Sort by increasing key
                sortedDeltaLengths = { key: val for key, val in sorted(deltaLengths.items(), key = lambda elem: elem[0]) }

                usedLength = 0

                for _, deltaLength in sortedDeltaLengths.items():
                    usedLength += deltaLength

                    if usedLength > instance.boxes[boxID].length:
                        print('Unfeasible: Compartment ' + str(compartmentID) + ' in shelf ' + str(shelfID) + ' in box ' + str(boxID) + ' doesn\'t satisfy length constraints.')
                        isFeasible = False

    result = CheckerOutput()
    result.isFeasible = isFeasible
    result.cost = cost

    return result

def calculateInstanceIndicators(instance):
    instanceIndicators = InstanceIndicators()

    instanceIndicators.nbBoxes = len(instance.boxes)
    instanceIndicators.nbItems = len(instance.items)

    firstKey = next(iter(instance.boxes))
    firstBox = instance.boxes[firstKey]

    instanceIndicators.minHeightBoxes = firstBox.height
    instanceIndicators.meanHeightBoxes = 0
    instanceIndicators.maxHeightBoxes = firstBox.height
    heights = []
    instanceIndicators.varianceHeightBoxes = 0

    instanceIndicators.minWidthBoxes = firstBox.width
    instanceIndicators.meanWidthBoxes = 0
    instanceIndicators.maxWidthBoxes = firstBox.width
    widths = []
    instanceIndicators.varianceWidthBoxes = 0

    instanceIndicators.minLengthBoxes = firstBox.length
    instanceIndicators.meanLengthBoxes = 0
    instanceIndicators.maxLengthBoxes = firstBox.length
    lengths = []
    instanceIndicators.varianceLengthsBoxes = 0

    instanceIndicators.minVolumeBoxes = firstBox.height * firstBox.width * firstBox.length
    instanceIndicators.meanVolumeBoxes = 0
    instanceIndicators.maxVolumeBoxes = firstBox.height * firstBox.width * firstBox.length
    instanceIndicators.totalVolumeBoxes = 0
    volumes = []
    instanceIndicators.varianceVolumeBoxes = 0

    for boxID in instance.boxes:
        box = instance.boxes[boxID]

        instanceIndicators.minHeightBoxes = min(instanceIndicators.minHeightBoxes, box.height)
        instanceIndicators.meanHeightBoxes += box.height
        instanceIndicators.maxHeightBoxes = max(instanceIndicators.maxHeightBoxes, box.height)
        heights += [box.height]

        instanceIndicators.minWidthBoxes = min(instanceIndicators.minWidthBoxes, box.width)
        instanceIndicators.meanWidthBoxes += box.width
        instanceIndicators.maxWidthBoxes = max(instanceIndicators.maxWidthBoxes, box.width)
        widths += [box.width]

        instanceIndicators.minLengthBoxes = min(instanceIndicators.minLengthBoxes, box.length)
        instanceIndicators.meanLengthBoxes += box.length
        instanceIndicators.maxLengthBoxes = max(instanceIndicators.maxLengthBoxes, box.length)
        lengths += [box.length]

        boxVolume = box.height * box.width * box.length

        instanceIndicators.minVolumeBoxes = min(instanceIndicators.minVolumeBoxes, boxVolume)
        instanceIndicators.meanVolumeBoxes += boxVolume
        instanceIndicators.maxVolumeBoxes = max(instanceIndicators.maxVolumeBoxes, boxVolume)
        instanceIndicators.totalVolumeBoxes += boxVolume
        volumes += [boxVolume]

    instanceIndicators.meanHeightBoxes /= instanceIndicators.nbBoxes
    instanceIndicators.varianceHeightBoxes = variance(heights) if instanceIndicators.nbBoxes >= 2 else 0

    instanceIndicators.meanWidthBoxes /= instanceIndicators.nbBoxes
    instanceIndicators.varianceWidthBoxes = variance(widths) if instanceIndicators.nbBoxes >= 2 else 0

    instanceIndicators.meanLengthBoxes /= instanceIndicators.nbBoxes
    instanceIndicators.varianceLengthItems = variance(lengths) if instanceIndicators.nbBoxes >= 2 else 0

    instanceIndicators.meanVolumeBoxes /= instanceIndicators.nbBoxes
    instanceIndicators.varianceVolumeBoxes = variance(volumes) if instanceIndicators.nbBoxes >= 2 else 0

    instanceIndicators.nbTimePeriods = 0

    for itemID in instance.items:
        item = instance.items[itemID]
        instanceIndicators.nbTimePeriods = max(instanceIndicators.nbTimePeriods, item.start + item.duration)

    firstKey = next(iter(instance.items))
    firstItem = instance.items[firstKey]

    instanceIndicators.minHeightItems = firstItem.height
    instanceIndicators.meanHeightItems = 0
    instanceIndicators.maxHeightItems = firstItem.height
    instanceIndicators.varianceHeightItems = 0

    instanceIndicators.minWidthItems = firstItem.width
    instanceIndicators.meanWidthItems = 0
    instanceIndicators.maxWidthItems = firstItem.width
    instanceIndicators.varianceWidthItems = 0

    instanceIndicators.minLengthItems = firstItem.length
    instanceIndicators.meanLengthItems = 0
    instanceIndicators.maxLengthItems = firstItem.length
    instanceIndicators.varianceLengthItems = 0

    instanceIndicators.minProfitItems = firstItem.profit
    instanceIndicators.meanProfitItems = 0
    instanceIndicators.meanProfitItemsPerTimePeriod = 0
    instanceIndicators.maxProfitItems = firstItem.profit
    instanceIndicators.varianceProfitItems = 0

    instanceIndicators.minVolumeItems = firstItem.height * firstItem.width * firstItem.length
    instanceIndicators.meanVolumeItems = 0
    instanceIndicators.meanVolumeItemsPerTimePeriod = 0
    instanceIndicators.maxVolumeItems = firstItem.height * firstItem.width * firstItem.length
    instanceIndicators.totalVolumeItems = 0
    instanceIndicators.varianceVolumeItems = 0

    instanceIndicators.nbHeights = 0
    instanceIndicators.nbWidths = 0
    instanceIndicators.nbItemsPerWidth = {}

    heights = []
    widths = []
    lengths = []
    profits = []
    volumes = []

    instanceIndicators.volumeItemsPerWidth = {}
    instanceIndicators.varianceVolumeItemsPerWidth = 0

    for itemID in instance.items:
        item = instance.items[itemID]

        instanceIndicators.minHeightItems = min(instanceIndicators.minHeightItems, item.height)
        instanceIndicators.meanHeightItems += item.height
        instanceIndicators.maxHeightItems = max(instanceIndicators.maxHeightItems, item.height)

        instanceIndicators.minWidthItems = min(instanceIndicators.minWidthItems, item.width)
        instanceIndicators.meanWidthItems += item.width
        instanceIndicators.maxWidthItems = max(instanceIndicators.maxWidthItems, item.width)

        instanceIndicators.minLengthItems = min(instanceIndicators.minLengthItems, item.length)
        instanceIndicators.meanLengthItems += item.length
        instanceIndicators.maxLengthItems = max(instanceIndicators.maxLengthItems, item.length)

        instanceIndicators.minProfitItems = min(instanceIndicators.minProfitItems, item.profit)
        instanceIndicators.meanProfitItems += item.profit
        instanceIndicators.maxProfitItems = max(instanceIndicators.maxProfitItems, item.profit)

        itemVolume = item.height * item.width * item.length

        instanceIndicators.minVolumeItems = min(instanceIndicators.minVolumeItems, itemVolume)
        instanceIndicators.meanVolumeItems += itemVolume
        instanceIndicators.maxVolumeItems = max(instanceIndicators.maxVolumeItems, itemVolume)
        instanceIndicators.totalVolumeItems += itemVolume

        if item.height not in heights:
            instanceIndicators.nbHeights += 1

        if item.width not in widths:
            instanceIndicators.nbWidths += 1

        heights += [item.height]
        widths += [item.width]
        lengths += [item.length]
        profits += [item.profit]
        volumes += [itemVolume]

        if item.width not in instanceIndicators.volumeItemsPerWidth:
            instanceIndicators.volumeItemsPerWidth[item.width] = 0

        instanceIndicators.volumeItemsPerWidth[item.width] += itemVolume

        if item.width not in instanceIndicators.nbItemsPerWidth:
            instanceIndicators.nbItemsPerWidth[item.width] = 0

        instanceIndicators.nbItemsPerWidth[item.width] += 1

    instanceIndicators.meanHeightItems /= instanceIndicators.nbItems
    instanceIndicators.varianceHeightItems = variance(heights) if instanceIndicators.nbItems >= 2 else 0

    instanceIndicators.meanWidthItems /= instanceIndicators.nbItems
    instanceIndicators.varianceWidthItems = variance(widths) if instanceIndicators.nbItems >= 2 else 0

    instanceIndicators.meanLengthItems /= instanceIndicators.nbItems
    instanceIndicators.varianceLengthItems = variance(lengths) if instanceIndicators.nbItems >= 2 else 0

    instanceIndicators.sumProfitsItems = instanceIndicators.meanProfitItems
    instanceIndicators.meanProfitItems /= instanceIndicators.nbItems
    instanceIndicators.meanProfitItemsPerTimePeriod = sum(profits) / instanceIndicators.nbTimePeriods
    instanceIndicators.varianceProfitItems = variance(profits) if instanceIndicators.nbItems >= 2 else 0

    instanceIndicators.meanVolumeItems /= instanceIndicators.nbItems
    instanceIndicators.meanVolumeItemsPerTimePeriod = instanceIndicators.totalVolumeItems / instanceIndicators.nbTimePeriods
    instanceIndicators.varianceVolumeItems = variance(volumes) if instanceIndicators.nbItems >= 2 else 0
    instanceIndicators.varianceVolumeItemsPerWidth = variance(instanceIndicators.volumeItemsPerWidth) if instanceIndicators.nbWidths >= 2 else 0

    eventsPerWidth = {}
    eventsDeltaLengthPerWidth = {}

    instanceIndicators.nbCliques = 0
    instanceIndicators.nbCliquesPerWidth = {}

    instanceIndicators.nbNonTrivialCliques = 0
    instanceIndicators.nbNonTrivialCliquesPerWidth = {}

    instanceIndicators.minCliqueSize = 1000000
    instanceIndicators.meanCliqueSize = 0
    instanceIndicators.maxCliqueSize = 0

    instanceIndicators.minCliqueSizePerWidth = {}
    instanceIndicators.meanCliqueSizePerWidth = {}
    instanceIndicators.maxCliqueSizePerWidth = {}

    for timePeriod in range(instanceIndicators.nbTimePeriods + 1):
        for itemID, item in instance.items.items():
            if item.width not in eventsPerWidth:
                eventsPerWidth[item.width] = []
                eventsDeltaLengthPerWidth[item.width] = []

                instanceIndicators.nbCliquesPerWidth[item.width] = 0
                instanceIndicators.nbNonTrivialCliquesPerWidth[item.width] = 0

                instanceIndicators.minCliqueSizePerWidth[item.width] = 1000000
                instanceIndicators.meanCliqueSizePerWidth[item.width] = 0
                instanceIndicators.maxCliqueSizePerWidth[item.width] = 0

            if timePeriod == item.start + item.duration:
                eventsPerWidth[item.width] += ['out']
                eventsDeltaLengthPerWidth[item.width] += [-item.length]

        for itemID, item in instance.items.items():
            if timePeriod == item.start:
                eventsPerWidth[item.width] += ['in']
                eventsDeltaLengthPerWidth[item.width] += [item.length]

    for width in eventsPerWidth:
        previousAction = ''
        currentCliqueSize = 0
        currentCliqueSumOfLengths = 0
        for eventIndex in range(len(eventsPerWidth[width])):
            event = eventsPerWidth[width][eventIndex]

            currentCliqueSumOfLengths += eventsDeltaLengthPerWidth[width][eventIndex]

            if event == 'in':
                currentCliqueSize += 1

                previousAction = 'in'
            elif event == 'out':
                if previousAction == 'in':
                    instanceIndicators.nbCliques += 1
                    instanceIndicators.nbCliquesPerWidth[width] += 1

                    if currentCliqueSumOfLengths > firstBox.length:
                        instanceIndicators.nbNonTrivialCliques += 1
                        instanceIndicators.nbNonTrivialCliquesPerWidth[width] += 1

                    instanceIndicators.minCliqueSizePerWidth[width] = min(instanceIndicators.minCliqueSizePerWidth[width], currentCliqueSize)
                    instanceIndicators.meanCliqueSizePerWidth[width] += currentCliqueSize
                    instanceIndicators.maxCliqueSizePerWidth[width] = max(instanceIndicators.maxCliqueSizePerWidth[width], currentCliqueSize)

                    instanceIndicators.minCliqueSize = min(instanceIndicators.minCliqueSize, currentCliqueSize)
                    instanceIndicators.meanCliqueSize += currentCliqueSize
                    instanceIndicators.maxCliqueSize = max(instanceIndicators.maxCliqueSize, currentCliqueSize)

                currentCliqueSize -= 1

                previousAction = 'out'

    instanceIndicators.meanCliqueSize /= instanceIndicators.nbTimePeriods

    for width in eventsPerWidth:
        instanceIndicators.meanCliqueSizePerWidth[width] /= instanceIndicators.nbCliquesPerWidth[width]

    instanceIndicators.covarianceProfitsLengths = 0

    profits = []
    lengths = []

    for itemID in instance.items:
        item = instance.items[itemID]

        profits += [item.profit]
        lengths += [item.length]

    instanceIndicators.covarianceProfitsLengths = covariance(profits, lengths) if instanceIndicators.nbItems >= 2 else 0

    return instanceIndicators

def calculateSolutionIndicators(instance, instanceIndicators, solution):
    solutionIndicators = SolutionIndicators()

    firstKey = next(iter(instance.boxes))
    firstBox = instance.boxes[firstKey]

    solutionIndicators.minNbShelvesPerBox = len(solution.boxesShelves[firstBox.ID]) if firstBox.ID in solution.boxesShelves else 0
    solutionIndicators.meanNbShelvesPerBox = 0
    solutionIndicators.maxNbShelvesPerBox = len(solution.boxesShelves[firstBox.ID]) if firstBox.ID in solution.boxesShelves else 0

    solutionIndicators.minNbCompartmentsPerBox = 0
    solutionIndicators.meanNbCompartmentsPerBox = 0
    solutionIndicators.maxNbCompartmentsPerBox = 0

    if firstBox.ID in solution.boxesShelves:
        for shelfID, _ in solution.boxesShelves[firstBox.ID].items():
            solutionIndicators.minNbCompartmentsPerBox += len(solution.boxesShelvesCompartments[firstBox.ID][shelfID])
            solutionIndicators.maxNbCompartmentsPerBox += len(solution.boxesShelvesCompartments[firstBox.ID][shelfID])

    solutionIndicators.nbAssignedItems = 0
    solutionIndicators.nbAssignedItemsPerWidth = {}

    for width in instanceIndicators.nbItemsPerWidth:
        solutionIndicators.nbAssignedItemsPerWidth[width] = 0

    solutionIndicators.volumeAssignedItems = 0
    solutionIndicators.volumeAssignedItemsPerTimePeriod = {}

    for timePeriod in range(instanceIndicators.nbTimePeriods + 1):
        solutionIndicators.volumeAssignedItemsPerTimePeriod[timePeriod] = 0

    for boxID, _ in instance.boxes.items():
        if boxID not in solution.boxesShelves:
            continue

        nbShelves = len(solution.boxesShelves[boxID])

        solutionIndicators.minNbShelvesPerBox = min(solutionIndicators.minNbShelvesPerBox, nbShelves)
        solutionIndicators.meanNbShelvesPerBox += nbShelves
        solutionIndicators.maxNbShelvesPerBox = max(solutionIndicators.maxNbShelvesPerBox, nbShelves)

        nbCompartments = 0

        for shelfID, _ in solution.boxesShelves[boxID].items():
            nbCompartments += len(solution.boxesShelvesCompartments[boxID][shelfID])

            for compartmentID, _ in solution.boxesShelvesCompartments[boxID][shelfID].items():
                solutionIndicators.nbAssignedItems += len(solution.boxesShelvesCompartmentsItems[boxID][shelfID][compartmentID])

                for itemID in solution.boxesShelvesCompartmentsItems[boxID][shelfID][compartmentID]:
                    item = instance.items[itemID]

                    solutionIndicators.nbAssignedItemsPerWidth[item.width] += 1

                    solutionIndicators.volumeAssignedItems += item.height * item.width * item.length

                    for timePeriod in range(item.start, item.start + item.duration):
                        solutionIndicators.volumeAssignedItemsPerTimePeriod[timePeriod] += item.height * item.width * item.length

        solutionIndicators.minNbCompartmentsPerBox = min(solutionIndicators.minNbCompartmentsPerBox, nbCompartments)
        solutionIndicators.meanNbCompartmentsPerBox += nbCompartments
        solutionIndicators.maxNbCompartmentsPerBox = max(solutionIndicators.maxNbCompartmentsPerBox, nbCompartments)

    solutionIndicators.proportionAssignedItems = solutionIndicators.nbAssignedItems / instanceIndicators.nbItems

    for width in instanceIndicators.nbItemsPerWidth:
        solutionIndicators.proportionAssignedItemsPerWidth[width] = solutionIndicators.nbAssignedItemsPerWidth[width] / instanceIndicators.nbItemsPerWidth[width]

    eventsPerWidth = {}
    eventsDeltaLengthPerWidth = {}
    eventsItemsPerWidth = {}

    for timePeriod in range(instanceIndicators.nbTimePeriods + 1):
        for itemID, item in instance.items.items():
            if item.width not in eventsPerWidth:
                eventsPerWidth[item.width] = []
                eventsDeltaLengthPerWidth[item.width] = []
                eventsItemsPerWidth[item.width] = []

            if timePeriod == item.start + item.duration:
                eventsPerWidth[item.width] += ['out']
                eventsDeltaLengthPerWidth[item.width] += [-item.length]
                eventsItemsPerWidth[item.width] += [item.ID]

        for itemID, item in instance.items.items():
            if timePeriod == item.start:
                eventsPerWidth[item.width] += ['in']
                eventsDeltaLengthPerWidth[item.width] += [item.length]
                eventsItemsPerWidth[item.width] += [item.ID]

    isItemInANonTrivialClique = {}
    for itemID, item in instance.items.items():
        isItemInANonTrivialClique[itemID] = 0

    for width in eventsPerWidth:
        previousAction = ''
        currentClique = []
        currentCliqueSumOfLengths = 0

        for eventIndex in range(len(eventsPerWidth[width])):
            event = eventsPerWidth[width][eventIndex]

            currentCliqueSumOfLengths += eventsDeltaLengthPerWidth[width][eventIndex]

            if event == 'in':
                previousAction = 'in'

                currentClique += [eventsItemsPerWidth[width][eventIndex]]
            elif event == 'out':
                if previousAction == 'in':
                    if currentCliqueSumOfLengths > firstBox.length:
                        for itemID in currentClique:
                            isItemInANonTrivialClique[itemID] = 1

                previousAction = 'out'

                currentClique.remove(eventsItemsPerWidth[width][eventIndex])

    solutionIndicators.nbAssignedItemsInNonTrivialCliques = sum(isItemInANonTrivialClique.values())
    solutionIndicators.proporitionAssignedItemsInNonTrivialCliques = solutionIndicators.nbAssignedItemsInNonTrivialCliques / instanceIndicators.nbItems

    solutionIndicators.ratioVolumeAssignedItemsVolumeBoxes = solutionIndicators.volumeAssignedItems / instanceIndicators.totalVolumeBoxes

    solutionIndicators.ratioVolumeAssignedItemsVolumeBoxesPerTimePeriod = {}
    for timePeriod in range(instanceIndicators.nbTimePeriods + 1):
        solutionIndicators.ratioVolumeAssignedItemsVolumeBoxesPerTimePeriod[timePeriod] = solutionIndicators.volumeAssignedItemsPerTimePeriod[timePeriod] / instanceIndicators.totalVolumeBoxes

    return solutionIndicators

def main(argc, argv, verbose=False):
    if (argc <= 1):
        printUsage()
        return

    instance = readInstance(argv[1])

    instanceIndicators = calculateInstanceIndicators(instance)

    if verbose:
        print('# Instance quality indicators')

        print('NbBoxes', instanceIndicators.nbBoxes)
        print('NbItems', instanceIndicators.nbItems)

        if instanceIndicators.nbBoxes >= 2:
            print('MinHeightBoxes', instanceIndicators.minHeightBoxes)

        print('MeanHeightBoxes', instanceIndicators.meanHeightBoxes)

        if instanceIndicators.nbBoxes >= 2:
            print('MaxHeightBoxes', instanceIndicators.maxHeightBoxes)
            print('VarianceHeightBoxes', instanceIndicators.varianceHeightBoxes)

        if instanceIndicators.nbBoxes >= 2:
            print('MinWidthBoxes', instanceIndicators.minWidthBoxes)

        print('MeanWidthBoxes', instanceIndicators.meanWidthBoxes)

        if instanceIndicators.nbBoxes >= 2:
            print('MaxWidthBoxes', instanceIndicators.maxWidthBoxes)
            print('VarianceWidthBoxes', instanceIndicators.varianceWidthBoxes)

        if instanceIndicators.nbBoxes >= 2:
            print('MinLengthBoxes', instanceIndicators.minLengthBoxes)

        print('MeanLengthBoxes', instanceIndicators.meanLengthBoxes)

        if instanceIndicators.nbBoxes >= 2:
            print('MaxLengthBoxes', instanceIndicators.maxLengthBoxes)
            print('VarianceLengthBoxes', instanceIndicators.varianceLengthsBoxes)

        if instanceIndicators.nbBoxes >= 2:
            print('MinVolumeBoxes', instanceIndicators.minVolumeBoxes)

        print('MeanVolumeBoxes', instanceIndicators.meanVolumeBoxes)

        if instanceIndicators.nbBoxes >= 2:
            print('MaxVolumeBoxes', instanceIndicators.maxVolumeBoxes)
            print('TotalVolumeBoxes', instanceIndicators.totalVolumeBoxes)
            print('VarianceVolumeBoxes', instanceIndicators.varianceVolumeBoxes)

        print('NbTimePeriods', instanceIndicators.nbTimePeriods)

        if instanceIndicators.nbItems >= 2:
            print('MinHeightItems', instanceIndicators.minHeightItems)

        print('MeanHeightItems', instanceIndicators.meanHeightItems)

        if instanceIndicators.nbItems >= 2:
            print('MaxHeightItems', instanceIndicators.maxHeightItems)
            print('VarianceHeightItems', instanceIndicators.varianceHeightItems)

        if instanceIndicators.nbItems >= 2:
            print('MinWidthItems', instanceIndicators.minWidthItems)

        print('MeanWidthItems', instanceIndicators.meanWidthItems)

        if instanceIndicators.nbItems >= 2:
            print('MaxWidthItems', instanceIndicators.maxWidthItems)
            print('VarianceWidthItems', instanceIndicators.varianceWidthItems)

        if instanceIndicators.nbItems >= 2:
            print('MinLengthItems', instanceIndicators.minLengthItems)

        print('MeanLengthItems', instanceIndicators.meanLengthItems)

        if instanceIndicators.nbItems >= 2:
            print('MaxLengthItems', instanceIndicators.maxLengthItems)
            print('VarianceLengthItems', instanceIndicators.varianceLengthItems)

        if instanceIndicators.nbItems >= 2:
            print('MinProfitItems', instanceIndicators.minProfitItems)

        print('MeanProfitItems', instanceIndicators.meanProfitItems)
        print('MeanProfitItemsPerTimePeriod', instanceIndicators.meanProfitItemsPerTimePeriod)

        if instanceIndicators.nbItems >= 2:
            print('MaxProfitItems', instanceIndicators.maxProfitItems)
            print('VarianceProfitItems', instanceIndicators.varianceProfitItems)

        if instanceIndicators.nbItems >= 2:
            print('MinVolumeItems', instanceIndicators.minVolumeItems)

        print('MeanVolumeItems', instanceIndicators.meanVolumeItems)
        print('MeanVolumeItemsPerTimePeriod', instanceIndicators.meanVolumeItemsPerTimePeriod)

        if instanceIndicators.nbItems >= 2:
            print('MaxVolumeItems', instanceIndicators.maxVolumeItems)
            print('TotalVolumeItems', instanceIndicators.totalVolumeItems)
            print('VarianceVolumeItems', instanceIndicators.varianceVolumeItems)

        print('VolumeItemsPerWidth', end=' ')
        print(instanceIndicators.volumeItemsPerWidth)

        print('VarianceVolumeItemsPerWidth', instanceIndicators.varianceVolumeItemsPerWidth)

        print('NbHeights', instanceIndicators.nbHeights)
        print('NbWidths', instanceIndicators.nbWidths)

        print('NbItemsPerWidth', end=' ')
        print(instanceIndicators.nbItemsPerWidth)

        print('CovarianceProfitsLengths', instanceIndicators.covarianceProfitsLengths)

        print('NbCliques', instanceIndicators.nbCliques)
        print('NbCliquesPerWidth', instanceIndicators.nbCliquesPerWidth)

        print('NbNonTrivialCliques', instanceIndicators.nbNonTrivialCliques)
        print('NbNonTrivialCliquesPerWidth', instanceIndicators.nbNonTrivialCliquesPerWidth)

        print('MinCliqueSize', instanceIndicators.minCliqueSize)
        print('MeanCliqueSize', instanceIndicators.meanCliqueSize)
        print('MaxCliqueSize', instanceIndicators.maxCliqueSize)

        print('MinCliqueSizePerWidth:', instanceIndicators.minCliqueSizePerWidth)
        print('MeanCliqueSizePerWidth:', instanceIndicators.meanCliqueSizePerWidth)
        print('MaxCliqueSizePerWidth:', instanceIndicators.maxCliqueSizePerWidth)

        print('RatioVolumeItemsVolumeBoxes', instanceIndicators.totalVolumeItems / instanceIndicators.totalVolumeBoxes)

        print()

    if (argc == 3):
        solution = readSolution(argv[2], instance)

        result = checkSolution(instance, solution)

        if verbose:
            print('# Solution indicators')
            print('Feasible', 1 if result.isFeasible else 0)
            print('Cost', result.cost)
            print()

        solutionIndicators = calculateSolutionIndicators(instance, instanceIndicators, solution)

        if verbose:
            print('# Solution quality indicators')

            if instanceIndicators.nbBoxes >= 2:
                print('MinNbShelvesPerBox', solutionIndicators.minNbShelvesPerBox)

            print('MeanNbShelvesPerBox', solutionIndicators.meanNbShelvesPerBox)

            if instanceIndicators.nbBoxes >= 2:
                print('MaxNbShelvesPerBox', solutionIndicators.maxNbShelvesPerBox)

                print('NbShelvesPerBox', end=' ')
                print(solutionIndicators.nbShelvesPerBox)

            if instanceIndicators.nbBoxes >= 2:
                print('MinNbCompartmentsPerBox', solutionIndicators.minNbCompartmentsPerBox)

            print('MeanNbCompartmentsPerBox', solutionIndicators.meanNbCompartmentsPerBox)

            if instanceIndicators.nbBoxes >= 2:
                print('MaxNbCompartmentsPerBox', solutionIndicators.maxNbCompartmentsPerBox)

            print('NbAssignedItems', solutionIndicators.nbAssignedItems)
            print('ProportionAssignedItems', solutionIndicators.proportionAssignedItems)

            print('NbAssignedItemsPerWidth', solutionIndicators.nbAssignedItemsPerWidth)

            print('NbAssignedItemsInNonTrivialCliques', solutionIndicators.nbAssignedItemsInNonTrivialCliques)
            print('ProportionNbAssignedItemInNonTrivialCliques', solutionIndicators.proporitionAssignedItemsInNonTrivialCliques)

            print('ProportionAssignedItemsPerWidth', solutionIndicators.proportionAssignedItemsPerWidth)

            print('VolumeAssignedItems', solutionIndicators.volumeAssignedItems)
            print('VolumeAssignedItemsPerTimePeriod', solutionIndicators.volumeAssignedItemsPerTimePeriod)

            print('RatioVolumeAssignedItemsVolumeBoxes', solutionIndicators.volumeAssignedItems / instanceIndicators.totalVolumeBoxes)
            print('RatioVolumeAssignedItemsVolumeBoxesPerTimePeriod', solutionIndicators.ratioVolumeAssignedItemsVolumeBoxesPerTimePeriod)

if __name__ == "__main__":
    argc = len(sys.argv)
    argv = sys.argv

    main(argc, argv, True)
